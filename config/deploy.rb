$:.unshift './lib'
require 'mina/bundler'
require 'mina/rails'
require 'mina/git'
require 'mina/rsync'
require 'mina/defaults'
require 'mina/common'
require 'mina/nginx'
require 'mina/unicorn'
# require 'mina/rbenv'  # for rbenv support. (http://rbenv.org)
# require 'mina/rvm'    # for rvm support. (http://rvm.io)

Dir['lib/mina/environments/*.rb'].each { |f| load f }

# Basic settings:
#   domain       - The hostname to SSH to.
#   deploy_to    - Path to deploy into.
#   repository   - Git repo to clone from. (needed by mina/git)
#   branch       - Branch name to deploy. (needed by mina/git)

set :app, 'app'

set :deploy_to, '/var/www/app'
set :repository, 'git@bitbucket.org:hien_nguyen_asnet/mina-deploy-practice.git'
set :branch, 'master'
set :user, 'vagrant'
set :identity_file,   '/Volumes/Data/training/training-hiennguyen/rails/chef/rails-server-example/vagrant/.vagrant/machines/default/virtualbox/private_key'

# For system-wide RVM install.
#   set :rvm_path, '/usr/local/rvm/bin/rvm'
set :rsync_options,   ["-e ssh -i #{identity_file}", '--recursive', '--delete', '--delete-excluded']

# Manually create these paths in shared/ (eg: shared/config/database.yml) in your server.
# They will be linked in the 'deploy:link_shared_paths' step.
set :shared_paths, ['config/database.yml', 'log']

set :server, ENV['to'] || default_server
invoke :"env:#{server}"

# Optional settings:
#   set :user, 'foobar'    # Username in the server to SSH to.
#   set :port, '30000'     # SSH port number.
#   set :forward_agent, true     # SSH forward_agent.

# This task is the environment that is loaded for most commands, such as
# `mina deploy` or `mina rake`.
task :environment do
  # If you're using rbenv, use this to load the rbenv environment.
  # Be sure to commit your .rbenv-version to your repository.
  # invoke :'rbenv:load'

  # For those using RVM, use this to load an RVM version@gemset.
  invoke :'rvm:use[ruby-2.0.0-p598@default]'
end

# Put any custom mkdir's in here for when `mina setup` is ran.
# For Rails apps, we'll make some of the shared paths that are shared between
# all releases.
task :setup => :environment do
  queue! %[sudo mkdir -p "#{deploy_to}/#{shared_path}/log"]
  queue! %[sudo chmod g+rx,u+rwx "#{deploy_to}/#{shared_path}/log"]

  queue! %[sudo mkdir -p "#{deploy_to}/#{shared_path}/config"]
  queue! %[sudo chmod g+rx,u+rwx "#{deploy_to}/#{shared_path}/config"]

  queue! %[sudo mkdir -p "#{deploy_to}/shared/log"]
  queue! %[sudo chmod g+rx,u+rwx "#{deploy_to}/shared/log"]

  queue! %[sudo mkdir -p "#{deploy_to}/shared/tmp/sockets"]
  queue! %[sudo chmod g+rwx,u+rwx "#{deploy_to}/shared/tmp/sockets"]

  queue! %[sudo mkdir -p "#{deploy_to}/#{shared_path}/deploy"]
  queue! %[sudo chmod g+rx,u+rwx "#{deploy_to}/#{shared_path}/deploy"]

  # queue! %[touch "#{deploy_to}/#{shared_path}/config/database.yml"]
  # queue  %[echo "-----> Be sure to edit '#{deploy_to}/#{shared_path}/config/database.yml'."]
end

desc "Deploys the current version to the server."
task :deploy => :environment do
  deploy do
    # Put things that will set up an empty directory into a fully set-up
    # instance of your project.
    # invoke :'git:clone'
    invoke :'deploy:link_shared_paths'
    invoke :'rsync:deploy'

    invoke :environment
    # invoke :'bundle:install'
    # invoke :'rails:db_migrate'
    # invoke :'rails:assets_precompile'
    # invoke :'deploy:cleanup'

    to :launch do
      queue "mkdir -p #{deploy_to}/#{current_path}/tmp/"
      queue "touch #{deploy_to}/#{current_path}/tmp/restart.txt"
    end
  end
end

# For help in making your deploy script, see the Mina documentation:
#
#  - http://nadarei.co/mina
#  - http://nadarei.co/mina/tasks
#  - http://nadarei.co/mina/settings
#  - http://nadarei.co/mina/helpers


###########################################################################
# Unicorn Tasks
###########################################################################

namespace :unicorn do
  desc "Upload and update (link) all Unicorn config files"
  task :update => [:upload]

  desc "Parses all Unicorn config files and uploads them to server"
  task :upload => [:'upload:config']

  namespace :upload do
    desc "Parses Unicorn config file and uploads it to server"
    task :config do
      upload_template 'Unicorn config', 'unicorn.rb', "#{config_path}/unicorn.rb"
    end
  end

  desc "Parses all Unicorn config files and shows them in output"
  task :parse => [:'parse:config', :'parse:script']

  namespace :parse do
    desc "Parses Unicorn config file and shows it in output"
    task :config do
      puts "#"*80
      puts "# unicorn.rb"
      puts "#"*80
      puts erb("#{config_templates_path}/unicorn.rb.erb")
    end
  end

  desc "Start unicorn"
  task :start do
    queue 'echo "-----> Start Unicorn"'
    queue echo_cmd "cd #{deploy_to}/#{current_path} && unicorn_rails -c #{config_path}/unicorn.rb -D"
  end

end

task :setup do
  queue %{echo "-----> Server #{nginx_config}"}
  invoke :create_extra_paths
  invoke :'nginx:upload'
  invoke :'unicorn:upload'
  invoke :'nginx:setup'
  invoke :'nginx:link'
end

task :run do
  invoke :'unicorn:start'
  invoke :'nginx:restart'
end

desc 'Create extra paths for shared configs, pids, sockets, etc.'
task :create_extra_paths do
  queue 'echo "-----> Create configs path"'
  queue echo_cmd "sudo mkdir -p #{config_path}"

  queue 'echo "-----> Create shared paths"'
  shared_dirs = shared_paths.map { |file| File.dirname("#{deploy_to}/#{shared_path}/#{file}") }.uniq
  cmds = shared_dirs.map do |dir|
    queue echo_cmd %{mkdir -p "#{dir}"}
  end

  queue 'echo "-----> Create PID and Sockets paths"'
  queue echo_cmd "sudo mkdir -p #{pids_path} && sudo chown #{user}:#{group} #{pids_path} && chmod +rw #{pids_path}"
  queue echo_cmd "sudo mkdir -p #{sockets_path} && chown #{user}:#{group} #{sockets_path} && chmod +rw #{sockets_path}"
end

def upload_template(desc, tpl, destination)
  contents = parse_template(tpl)
  queue %{echo "-----> Put #{desc} file to #{destination}"}
  queue %{echo "#{contents}" > #{destination}}
  queue check_exists(destination)
end

task :sudo do
  set :sudo, true
  set :term_mode, :system # :pretty doesn't seem to work with sudo well
end

def parse_template(file)
  erb("#{config_templates_path}/#{file}.erb").gsub('"','\\"').gsub('`','\\\\`').gsub('$','\\\\$')
end

def check_response
  'then echo "----->   SUCCESS"; else echo "----->   FAILED"; fi'
end

def check_exists(destination)
  %{if [[ -s "#{destination}" ]]; #{check_response}}
end

def check_symlink(destination)
  %{if [[ -h "#{destination}" ]]; #{check_response}}
end

# Allow to run some tasks as different (sudoer) user when sudo required
module Mina
  module Helpers
    def ssh_command
      args = domain!
      args = if sudo? && sudoer?
               "#{sudoer}@#{args}"
             elsif user?
               "#{user}@#{args}"
             end
      args << " -i #{identity_file}" if identity_file?
      args << " -p #{port}" if port?
      args << " -t"
      "ssh #{args}"
    end
  end
end
# FreeBSD
namespace :env do
  task :vagrant => [:environment] do
    # FIXME: Modify this before deploying
    set :domain,              '192.168.33.11'

    set :deploy_to,           '/var/www/app'
    set :sudoer,              'vagrant'
    set :user,                'vagrant'
    set :group,               'vagrant'
    set :services_path,       '/etc/init.d'          # where your God and Unicorn service control scripts will go
    set :nginx_path,          '/etc/nginx'
    set :deploy_server,       'vagrant'                   # just a handy name of the server

    invoke :defaults                                       # load rest of the config
  end
end
